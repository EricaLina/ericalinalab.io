---
layout: projects
title: Erica's Keyboards
show_collection: keyboards
description: >
      Erica designs, makes and programs keyboards.
menu: true
order: 6 
---

I rewired the matrix of a Radio Shack model 2 keyboard to work on a color computer
when I was a teenager. I developed RSI in 1998 and switched to a Kinesis advantage
with the Dvorak layout.

I still use Dvorak, and also Beakl 15, and Hands Down Neu. My QMK
firmware has over 70 alpha keyboard layouts to choose from, and supports
multiple languages. My QMK firmware is extensive and supports a variety of 
keyboard shapes and allows me to use the same keyboard maps and features everywhere.

I've been primarily interested in hand wiring contour keyboards 
and have made some circuit boards to facilitate making hand wired keyboards.

I am currently visiting a svalboard,
I have my franken kyria and my new hacked 54 key kinesis.
My qmk is always under construction it seems. and so it goes.

I am pursuing some really interesting ideas about a handwire
design that is easy to make. I have designs for new boards and new frames
for split or single contour hand wire keyboards.
Alone time with my computer awaits.
