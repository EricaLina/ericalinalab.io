---
layout: projects
title: Tango
show_collection: tango
description: >
      Erica's tango challenges, meditations, concepts and practices.
menu: true
order: 3
---

I have been dancing Argentine tango for many years and began teaching in
2010.

My style is apparently unique and I am a good dancer, I am told I feel
very safe. I can't dance with myself so I can't say. 
By the grace of my curiousity, experience and learning practices, 
I have developed a different sort of pedagogy
for learning tango and beingness in a somatic and experiential way. 

My teaching gives my students a mindful daily practice much like The Vade Mechum 
practices I use for flute. Mindfulness and meditation are at the core of my
teaching. The result is that the student not only learns tango, but how
to be in the world mindfully and intentionally through out their days

I like to use triggers and challenges to help students refocus on their
posture, movement and beingness throughout their day.

I have written many articles and challenges about tango over the years, 
[here](https://tangobreath.github.io) but I have more to say.

I have trigger challenges for you, meditations and practices and more.

