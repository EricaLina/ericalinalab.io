---
layout: resume
title: Résumé*
description: >
    The condensed resume of Erica Lina
  
hide_description: true
menu: true
order: 10
left_column:
 - work
 - volunteer
 - education
 - awards
 - publications
 - references
right_column:
 - languages
 - skills
 - interests
---
