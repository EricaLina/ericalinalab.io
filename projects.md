---
layout: projects
title: Projects
show_collection: projects
description: >
      This is just where I put my projects, like sculpture, and the random things
      I make that arent flutes or keyboards.
      I enjoy making sculpture and mobiles, and I'm also concerned with 
      sustainability, and ergonomics in this computer age world.
      
menu: true
order: 11
---
