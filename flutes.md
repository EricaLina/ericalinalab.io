---
layout: projects
title: Flutes
show_collection: flutes
description: >
      I have played and made flutes most of my life.

menu: true
order: 6
---

I began playing flute at 8 years old. 
I took lessons from a student of my grandfather's. 
I played flute academically into college. I never graduated. 
School was never for me. In some ways I'm just not fond of the classical
career path of the classical solo flutist. I was at the doorstep and walked away
for various reasons. Life.
I play piccolo, C flute, Alto, bass, shakuhachi and overtone flutes. 

I make and play long overtone pipes with a shakuhachi embouchure. 
Perhaps they are 'shakpipes'. A cylindrical pipe always plays
the even harmonic series.
My favorite plays a b2, I have others at G#2, and Eb2. I have been
making headjoints to facilitate creating these long overtone flutes.
I'm very interested in how these will develop.

I am, as usual, interested in the far reaches of the flute. 
I was into extended techniques and multiphonics many decades ago. 
That continues today with my overtone flutes, my flute designs,
and my music.
I just bought a looper and an effects pedal and I'm very interested in
how extendend techniques will combine with electronics. 

I have also studied the evolution and mechanics of the modern flute.
My opinon is that an open G and all that goes with the Carte 67 is the better design
than the common modern bastardization of the boehm system flute we have now
with its closed G, Garibaldi thumb mechanic and no f# key. The modern flute
is a chain of compromises in tone which start with the closed G.

One of my goals is to build a modern flute of my own design. I am working
towards that.

