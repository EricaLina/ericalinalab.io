---
layout: projects
title: Well being
show_collection: well-being
description: >
      Erica's questions, philosophy and meditations for living and being.

menu: true
order: 2
---

I meditate a lot, I make my own meditations, and I use various techniques
to introduce mindful meditation in the things I do throughout the day.
I have a history with Tai chi, Ba gua, Xing Yi, Kung fu, and other martial arts.
I still very much like doing different walking and movement meditations.

It all blends a little with my Tango, but for now here we are.
