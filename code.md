---
layout: projects
title: Erica's Code
show_collection: code
description: >
      Coding since the 70's.  Here's some of it.
menu: true
order: 4
---
The things that Erica codes.
{:.lead}

<!-- ![Screenshot](assets/img/blog/hydejack-8.png){:.lead} -->
<!-- Hydejack's cover page on a variety of screens. -->
<!-- {:.figure} -->

I know many languages, C, Elisp, Haskell, python, zsh and Unix tools seem to
be consistently at the top of my usage list. Curious to try babashka
and common lisp as tools to build things. I also like creating languages.
I prefer functional languages and lisps. I built a 4 year project in Clojure 
and it remains one of my most favorite languages.

My prefered computer system is [Arch Linux](https://archlinux.org) or [Armbian](https://armbian.org)
with zsh, my window manager is [XMonad](https://xmonad.org), 
which is written in [Haskell](https://haskell.org).
 My [Xmonad configuration is here](https://gitlab.com/EricaLina/xmonad-setup).
 
My work  environment is a [custom configuration](https://gitlab.com/ericalina/Ericas-emacs.git) in [Emacs](https://gnu.org/software/emacs) with [Evil](https://emacswiki.org/emacs/Evil).  I use _Emacs_ for everything.  I install and manage my _Emacs_ development environment with a [custom tool called Emacsn](https://gitlab.com/ericalina/Emacsn.git). __Emacsn__ also a great way to try _Emacs_.
 __Emacsn__ gives many configurations to choose from and try.

I also design and build computer keyboards using ARM processors. I have an 
[extensive firmware](https://gitlab.com/ericalina/MyQmk.git) 
which is built on the [QMK firmware](https://docs.qmk.fm)

The Simple Process Repl [SPR](https://gitlab.com/ericalina/SPR), 
A simple but cool stackless programming language, application framework and 
REPL that I built on top of python and yaml.




