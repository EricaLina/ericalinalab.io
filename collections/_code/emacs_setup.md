---
layout: page
title: 'Emacs Configuration'
date: 27 Jul 2024
image: /assets/img/projects/hy-img.svg
screenshot: /assets/img/projects/hy-img.svg
links:
#  - title: Website
#    url: https://qwtel.com/hy-img/
  - title: Source
    url: https://gitlab.com/EricaLina/emacs-setup
caption: My Emacs setup, built from scratch.
description: >
  This is my emacs setup, written from scratch. 
accent_color: '#4fb1ba'
accent_image:
  background: 'linear-gradient(to bottom,#193747 0%,#233e4c 30%,#3c929e 50%,#d5d5d4 70%,#cdccc8 100%)'
  overlay:    true
---

  I have used emacs in some Vi mode since 1995. I've written a lot of elisp over the years.
  This is a nicely organized and simple emacs configuration which uses packages.
  Emacs has come a long way since version 17 where I started.

  I use emacs for everything, Coding, writing, publishing, tasks, calendar, email, and
  language learning. My emacs has a lot of bells and whistles.
