---
layout: page
title: 'Xmonad-setup'
date: 25 Dec 2018
image: /assets/img/projects/hy-push-state.svg
screenshot: /assets/img/projects/hy-push-state.svg
links:
  - title: Website
    url: https://xmonad.org/
  - title: Source
    url: https://gitlab.com/EricaLina/xmonad-setup
caption: My Xmonad window manager configuration (Haskell)
description: >
  My Xmonad configuration. 
  
accent_color: '#4fb1ba'
accent_image:
  background: 'linear-gradient(to bottom,#193747 0%,#233e4c 30%,#3c929e 50%,#d5d5d4 70%,#cdccc8 100%)'
  overlay:    true
---
  It is by far the best window manager I have ever used.
  This is my Xmonad window manager, written in haskell, the configuration
  compiles to an executable.

  This setup has polybar for its bar. Although I'm wondering why I need a bar.
  This configuration uses grid select for many things but also has hot key selections
  and context sensitive help for sub-menu hotkeys, like _which-key_ in Emacs. 

  The context sensitive help is unique but also very helpful to remember all of the
  possible hotkeys and what they do. 
  I wrote it completely in awk, It uses denzen to display the help.

     * topics/workspaces with autoloading applications.
     * named scratchpads.
       * urxvt 
       * bc - calculator
       * ghci - for haskell development
       * htop
       * dmenu
       * rofi
       * conky
     * Hot key Search
       * man
       * hackage
       * wikipedia
       * wiktionary
       * reverso
       * arch linux packages and AUR.
       * many others.
